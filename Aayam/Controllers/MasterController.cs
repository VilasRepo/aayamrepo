﻿using DAL.Repositories.Admin.Interface;
using DAL.Repositories.Admin.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.MasterDetails;

namespace Aayam.Controllers
{
    public class MasterController : Controller
    {
        private readonly IDAddMasterDetails _idAddMasterDetails;
        public MasterController()
        {
            _idAddMasterDetails = new DAddmasterDetails();
        }

        public ActionResult GetAllStreams()
        {
            ModelState.Clear();
            return View(_idAddMasterDetails.GetAllStreams());
        }
      
        public ActionResult AddStream()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddStream(Streams objStream)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_idAddMasterDetails.addStream(objStream))
                    {
                        ViewBag.Message =true;
                    }
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

      
        public ActionResult updateStream(int id)
        {
          
            return View(_idAddMasterDetails.GetAllStreams().Find(str => str.StreamID == id));

        }
      
        [HttpPost]
        public ActionResult updateStream(int id, Streams objStream)
        {
            try
            {
                if ( _idAddMasterDetails.UpdateStream(id,objStream))
                {
                    ViewBag.message = true;  
                }
                return RedirectToAction("GetAllStreams");
            }
            catch
            {
                return View();
            }
        }
      
        public ActionResult DeleteStream(int id)
        {
            try
            {
              
                if (_idAddMasterDetails.DeleteStream(id))
                {
                    ViewBag.message = true;

                }
                return RedirectToAction("GetAllStreams");

            }
            catch
            {
                return View();
            }
        }  
    
    }
}
