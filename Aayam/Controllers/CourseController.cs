﻿using DAL.Repositories.Master.Interface;
using DAL.Repositories.Master.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.MasterDetails;

namespace Aayam.Controllers
{
    public class CourseController : Controller
    {
        private readonly IDCourse _idCourse;
        public CourseController()
        {
            _idCourse = new DCourse();
        }

        public ActionResult CourseSelect()
        {
            ModelState.Clear();
            return View(_idCourse.CourseSelect());
        }

        public ActionResult CourseInsert()
        {
            ViewBag.Streams = _idCourse.BindStream();
            return View();
        }

        [HttpPost]
        public ActionResult CourseInsert(Course objCourse)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_idCourse.CourseInsert(objCourse))
                    {
                        ViewBag.Message =true;
                    }
                }
                ViewBag.Streams = _idCourse.BindStream();
                ModelState.Clear();
                return View();
               
            }
            catch
            {
                return View();
            }
        }

        public ActionResult CourseUpdate(int id)
        {
            ViewBag.Streams = _idCourse.BindStream();
            return View(_idCourse.CourseSelect().Find(str => str.CourseID == id));
        }

        [HttpPost]
        public ActionResult CourseUpdate(int id, Course objCourse)
        {
            try
            {
                if (_idCourse.CourseUpdate(id, objCourse))
                {
                    ViewBag.message = true;
                }
                return RedirectToAction("CourseSelect");
            }
            catch
            {
                return View();
            }
        }


        //public ActionResult BindStream()
        //{
        //    ModelState.Clear();
        //    return View(_idCourse.BindStream());
        //}
    }
}