﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Repositories.Student.Interface;
using DAL.Repositories.Student.Repository;
using ViewModel.StudentDetails;

namespace Aayam.Controllers
{
    public class StudentController : Controller
    {
        private readonly IDStudent _idstudent;

        public StudentController()
        {
            _idstudent = new DStudent();

        }

        //
        // GET: /Student/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Profile()
        {
            return View();
        }

        //
        // GET: /Student/
        public ActionResult getAllStudents()
        {
            return View();
        }
        public ActionResult getAllStudentsIsJoined()
        {

            return View(_idstudent.getAllStudentIsJoined());
        }
        public ActionResult getAllStudentsIsNotJoined()
        {

            return View(_idstudent.getAllStudentIsNotJoined());
        }
        public ActionResult getAllEnquiry()
        {
            return View(_idstudent.GetEnquiry());
        }
        public ActionResult addEnquiry()
        {
            ViewBag.Country = _idstudent.GetCountry();
            ViewBag.Course = _idstudent.GetCourse();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectToRouteResult addEnquiry(StudentViewModel objStudent)
        {
            if (ModelState.IsValid)
            {
                _idstudent.AddEnquiry(objStudent);
            }
            return RedirectToAction("getAllEnquiry", "Student");
        }
        public JsonResult GetState(int countryId)
        {
            return Json(_idstudent.GetState(countryId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCity(int stateId)
        {
            return Json(_idstudent.GetCity(stateId), JsonRequestBehavior.AllowGet);
        }
        public ActionResult moveStudentToAdmission(int id)
        {
            if (_idstudent.setStudentIsJoined(id))
                return RedirectToAction("getAllStudentsIsJoined", "Student");

            return RedirectToAction("", "");
        }
        public ActionResult moveStudentToNotJoined(int id)
        {
            if (_idstudent.setStudentIsNotJoined(id))
                return RedirectToAction("getAllStudentsIsNotJoined", "Student");

            return RedirectToAction("", "");
        }
    }
}