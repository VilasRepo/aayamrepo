﻿using System.Web;
using System.Web.Optimization;

namespace Aayam
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js","~/Scripts/datatable.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css"
            //          ));

            //bundles.Add(new StyleBundle("~/bundles/datatable").Include(
            //          "~/Scripts/DataTables/jquery.dataTables.min.js",
            //           "~/Scripts/DataTables/dataTables.bootstrap.min.js"));
               

            bundles.Add(new ScriptBundle("~/bundles/jqueryBhaiya").Include(
                     //"~/Bhaiya/Scripts/jquery.min.js",                     
                     "~/Bhaiya/Scripts/bootstrap.min.js",
                     "~/Bhaiya/Scripts/animsition.min.js",
                     "~/Bhaiya/Scripts/jquery-asScroll.min",
                     "~/Bhaiya/Scripts/jquery.mousewheel.min.js",
                    "~/Bhaiya/Scripts/jquery.asScrollable.all.min.js",
                    "~/Bhaiya/Scripts/jquery-asHoverScroll.min.js",
                    "~/Bhaiya/Scripts/core.min.js",
                    "~/Bhaiya/Scripts/site.min.js",
                    "~/Bhaiya/Scripts/menu.min.js",
                    //"~/Bhaiya/Scripts/menubar.min.js",
                    //"~/Bhaiya/Scripts/sidebar.min.js",

                    "~/Bhaiya/Scripts/config-colors.min.js",
                    "~/Bhaiya/Scripts/config-tour.min.js",
                    "~/Bhaiya/Scripts/asscrollable.min.js",
                    "~/Bhaiya/Scripts/animsition.min(1).js",
                    "~/Bhaiya/Scripts/slidepanel.min.js",
                    "~/Bhaiya/Scripts/switchery.min(1).js",
                    "~/Bhaiya/Scripts/screenfull.min.js",
                    "~/Bhaiya/Scripts/jquery.placeholder.min.js",
                    "~/Bhaiya/Scripts/input-group-file.min.js",
                    "~/Bhaiya/Scripts/panel.js",
                    "~/Bhaiya/Scripts/panel-actions.js",
                    "~/Bhaiya/Scripts/bootstrap-select.js",
                    "~/Bhaiya/Scripts/bootstrap-datepicker.js",
                    "~/Bhaiya/Scripts/bootstrap-datepicker_002.js",
                    "~/Bhaiya/Scripts/responsive-tabs.js",
                    "~/Bhaiya/Scripts/tabs.js",
                    "~/Bhaiya/Scripts/jquery-ui.min.js"
                     ));

            bundles.Add(new StyleBundle("~/Bhaiya/css").Include(
                        "~/Bhaiya/Styles/bootstrap.min.css",
                        "~/Bhaiya/Styles/bootstrap-extend.min.css",
                        "~/Bhaiya/Styles/site.min.css",
                        //"~/Bhaiya/Styles/teal.css",
                        "~/Bhaiya/Styles/indigo.css",
                        
                        "~/Bhaiya/Styles/team.css",
                        "~/Bhaiya/Styles/animsition.min.css",
                        "~/Bhaiya/Styles/asScrollable.min.css",
                        "~/Bhaiya/Styles/switchery.min.css",
                        "~/Bhaiya/Styles/introjs.min.css",
                        "~/Bhaiya/Styles/slidePanel.min.css",
                        "~/Bhaiya/Styles/flag-icon.min.css",
                        "~/Bhaiya/Styles/modals.css",
                        "~/Bhaiya/Styles/Pagination.css",
                        "~/Bhaiya/Styles/bootstrap-table.css",
                        "~/Bhaiya/Styles/Profile.css",
                        "~/Bhaiya/Styles/bootstrap-select.css",
                        "~/Bhaiya/Styles/bootstrap-datepicker.css",
                        "~/Bhaiya/Styles/sweet-alert.css",
                        "~/Bhaiya/Styles/bootstrap-markdown.css",
                        "~/Bhaiya/Styles/select2.css",
                        "~/Bhaiya/Styles/mailbox.css",
                        "~/Bhaiya/Styles/user.css",
                        "~/Bhaiya/Styles/web-icons.css",
                        "~/Bhaiya/Styles/brand-icons.min.css",
                        "~/Bhaiya/Styles/css.css",
                        "~/Bhaiya/Styles/font-awesome.css",
                        "~/Bhaiya/Styles/font-awesome.min.css",
                        "~/Bhaiya/Styles/glyphicons.css",
                        "~/Bhaiya/Styles/jquery-ui.css"                  
                     ));

        }
    }
}


//"~\Bhaiya\Scripts\bootstrap-table-mobile.js",
//"~\Bhaiya\Scripts\bootstrap-table.js",
//"~\Bhaiya\Scripts\bootstrap.min.js",
//"~\Bhaiya\Scripts\breakpoints.min.js",
//"~\Bhaiya\Scripts\editor.js",
//"~\Bhaiya\Scripts\intro.min.js",
//"~\Bhaiya\Scripts\jquery-asScroll.min.js",
//"~\Bhaiya\Scripts\jquery-placeholder.min.js",
//"~\Bhaiya\Scripts\jquery-slidePanel.min.js",
//"~\Bhaiya\Scripts\jquery.min.js",
//"~\Bhaiya\Scripts\jquery.mousewheel.min.js",
//"~\Bhaiya\Scripts\modernizr.min.js",

//"~\Bhaiya\Scripts\screenfull.min.js",
//"~\Bhaiya\Scripts\skintools.min.js",

//"~\Bhaiya\Scripts\switchery.min.js",
//"~\Bhaiya\Scripts\toolbar.js",
//"~\Bhaiya\Scripts\tooltip-popover.js",
//"~\Bhaiya\Scripts\webui-popover.js",

 //"~/Bhaiya/Styles/bootstrap.min.css",
 //                    //"~/Bhaiya/Styles/teal.css",
 //                    "~/Bhaiya/Styles/indigo.css",

 //                   "~/Bhaiya/Styles/animsition.min.css",
 //                   "~/Bhaiya/Styles/asScrollable.min.css",
 //                   "~/Bhaiya/Styles/bootstrap-datepicker.css",
 //                   "~/Bhaiya/Styles/bootstrap-extend.min.css",
 //                   "~/Bhaiya/Styles/bootstrap-markdown.css",
 //                   "~/Bhaiya/Styles/bootstrap-select.css",
 //                   "~/Bhaiya/Styles/bootstrap-table.css",
 //                   "~/Bhaiya/Styles/bootstrap.min.css",
 //                   "~/Bhaiya/Styles/brand-icons.min.css",
 //                   "~/Bhaiya/Styles/css.css",
 //                   "~/Bhaiya/Styles/Customised.css",
 //                   "~/Bhaiya/Styles/editor.css",
 //                   "~/Bhaiya/Styles/flag-icon.min.css",
 //                   "~/Bhaiya/Styles/font-awesome.css",
 //                   "~/Bhaiya/Styles/font-awesome.min.css",
                    
 //                   "~/Bhaiya/Styles/introjs.min.css",
 //                   "~/Bhaiya/Styles/jquery-ui.css",
 //                   "~/Bhaiya/Styles/login-v2.css",
 //                   "~/Bhaiya/Styles/mailbox.css",
 //                   "~/Bhaiya/Styles/modals.css",
 //                   "~/Bhaiya/Styles/Pagination.css",
 //                   "~/Bhaiya/Styles/profile.css",
 //                   "~/Bhaiya/Styles/select2.css",
 //                   "~/Bhaiya/Styles/site.min.css",
 //                   "~/Bhaiya/Styles/skintools.min.css",
 //                   "~/Bhaiya/Styles/slidePanel.min.css",
 //                   "~/Bhaiya/Styles/sweet-alert.css",
 //                   "~/Bhaiya/Styles/switchery.min.css",
 //                   "~/Bhaiya/Styles/team.css",
 //                   "~/Bhaiya/Styles/toolbar.css",
 //                   "~/Bhaiya/Styles/user.css",
 //                   "~/Bhaiya/Styles/web-icons.css",
 //                   "~/Bhaiya/Styles/web-icons.min.css",
 //                   "~/Bhaiya/Styles/webui-popover.css",
