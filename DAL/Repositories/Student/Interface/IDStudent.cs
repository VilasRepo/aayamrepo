﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.MasterDetails;
using ViewModel.StudentDetails;

namespace DAL.Repositories.Student.Interface
{
    public interface IDStudent
    {
        List<StudentViewModel> getAllStudentIsJoined();
        List<StudentViewModel> getAllStudentIsEnquiry();
        List<StudentViewModel> getAllStudentIsNotJoined();
        bool setStudentIsJoined(int id);
        bool setStudentIsNotJoined(int id);
        bool AddEnquiry(StudentViewModel stu);

        List<StudentViewModel> GetEnquiry();

        List<CountryViewModel> GetCountry();

        List<StateViewModel> GetState(int countryId);

        List<CityViewModel> GetCity(int stateId);

        List<Course> GetCourse();
    }
}
