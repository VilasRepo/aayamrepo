﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.MasterDetails;

namespace DAL.Repositories.Master.Interface
{
   public interface IDCourse
    {
        bool CourseInsert(Course objCourse);
        List<CourseViewModel> CourseSelect();
        bool CourseUpdate(int id, Course objCourse);
        bool CourseDelete(int Id);
        List<Streams> BindStream();
    }
}
