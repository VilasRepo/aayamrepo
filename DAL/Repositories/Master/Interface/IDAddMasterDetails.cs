﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.MasterDetails;

namespace DAL.Repositories.Admin.Interface
{
    public interface IDAddMasterDetails
    {
    //     bool addStream(string stream);
    //     bool addBatch(string Batch);
    //     bool addCourse(string Course);

         bool addStream(Streams objStream);
         List<Streams> GetAllStreams();
         bool UpdateStream(int id,Streams objStream);
         bool DeleteStream(int Id);

    }
}
