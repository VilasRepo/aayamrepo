﻿using DAL.Repositories.Admin.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using ViewModel.MasterDetails;
using System.Configuration;

namespace DAL.Repositories.Admin.Repository
{
    public class DAddmasterDetails : IDAddMasterDetails
    {
        public bool addStream(Streams objStream)
        {
            try
            {
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_AddNewStream", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Stream", objStream.StreamName);
                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();
                if (i >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public List<Streams> GetAllStreams()
        {
            try
            {
                List<Streams> StreamsList = new List<Streams>();
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_GetStreams", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                con.Open();
                da.Fill(dt);
                con.Close();
                foreach (DataRow dr in dt.Rows)
                {

                    StreamsList.Add(

                        new Streams
                        {

                            StreamID= Convert.ToInt32(dr["StreamID"]),
                            StreamName = Convert.ToString(dr["Stream"]),
                        }
                        );
                }
                return StreamsList;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }
        
        public bool UpdateStream(int id,Streams objStream)
        {

            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_UpdateStream", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@StreamId", id);
            com.Parameters.AddWithValue("@StreamName", objStream.StreamName);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteStream(int Id)
        {

            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_DeleteStreamByID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@StreamID", Id);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }   
    }
}
