﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.MasterDetails;
using System.Configuration;
using DAL.Repositories.Master.Interface;


namespace DAL.Repositories.Master.Repository
{
   public class DCourse : IDCourse
    {
        public bool CourseInsert(Course objCourse)
        {
            try
            {
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_CourseInsert", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Course", objCourse.CourseName);
                com.Parameters.AddWithValue("@StreamID", objCourse.StreamID);
                //com.Parameters.AddWithValue("@StreamID", objCourse.StreamID);
                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();
                if (i != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw new Exception();
            }

            //SqlParameter[] parameters = new SqlParameter[]
            //{
            //    new SqlParameter("@Course", objCourse.CourseName),
            //    new SqlParameter("@StreamID", objCourse.StreamID)
            //};
          //  objCourse.CourseID = Convert.ToInt32(ExecuteScalar(Factory.getSqlInstance().ToString(), CommandType.StoredProcedure, "sp_CourseInsert", parameters));

        }

        public List<CourseViewModel> CourseSelect()
        {
            try
            {
                List<CourseViewModel> CourseList = new List<CourseViewModel>();
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_CourseSelectAll", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                con.Open();
                da.Fill(dt);
                con.Close();
                foreach (DataRow dr in dt.Rows)
                {
                    CourseList.Add(
                        new CourseViewModel
                        {
                            CourseID = Convert.ToInt32(dr["CourseID"]),
                            CourseName = Convert.ToString(dr["Course"]),
                            StreamID = Convert.ToInt32(dr["StreamID"]),
                            Stream = Convert.ToString(dr["Stream"]),
                        }
                        );
                }
                return CourseList;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public bool CourseUpdate(int id, Course objCourse)
        {
            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_CourseUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CourseID", id);
            com.Parameters.AddWithValue("@Course", objCourse.CourseName);
            com.Parameters.AddWithValue("@StreamID", objCourse.StreamID);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CourseDelete(int Id)
        {
            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_CourseDelete", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CourseID", Id);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Streams> BindStream()
        {
            try
            {
                List<Streams> BindStream = new List<Streams>();
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_GetStreams", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                con.Open();
                da.Fill(dt);
                con.Close();
                foreach (DataRow dr in dt.Rows)
                {
                    BindStream.Add(
                        new Streams
                        {
                            StreamID = Convert.ToInt32(dr["StreamID"]),
                            StreamName = Convert.ToString(dr["Stream"]),
                        }
                        );
                }
                return BindStream;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
   
   }
}
