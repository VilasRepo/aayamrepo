﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.MasterDetails
{
    public class Course
    {
        public int CourseID { get; set; }

        [Required(ErrorMessage = "Enter Course")]
        [Display(Name = "Course")]
        public string CourseName { get; set; }
        
        [Required(ErrorMessage = "Select Stream")]
        [Display(Name = "Stream")]
        public int StreamID { get; set; }

    }

    public class CourseViewModel
    {
        public int CourseID { get; set; }

        [Required(ErrorMessage = "Enter Course")]
        [Display(Name = "Course")]
        public string CourseName { get; set; }

        public int StreamID { get; set; }
        public string Stream { get; set; }
    }


}
