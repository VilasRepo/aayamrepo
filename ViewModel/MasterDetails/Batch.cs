﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ViewModel.MasterDetails
{
    public class Batches
    {
        public int BatchID { get; set; }
       
        [Required(ErrorMessage = "This Field is required")]
        public string Batch { get; set; }
    }
}
