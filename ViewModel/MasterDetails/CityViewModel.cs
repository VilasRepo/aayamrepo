﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.MasterDetails
{
  public class CityViewModel
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
}
