﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ViewModel.MasterDetails
{
    public class Streams
    {
        [Display(Name = "ID")]
        public int StreamID { get; set; }
        [Required(ErrorMessage = "This Field is required")]
        [Display(Name = "Stream")]
        public string StreamName { get; set; }
    }
}
